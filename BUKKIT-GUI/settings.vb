﻿
Public Class settings
    Public demxp As String
    Public deport As String
    Public denether As String
    Public depvp As String
    Public deon As String
    Public wl As String
    Private Sub settings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fr = My.Computer.FileSystem.OpenTextFileReader("server.properties")

        config.Text = fr.ReadToEnd
        fr.Close()
        'maxplayer conf
        demxp = config.Text.Substring(config.Text.LastIndexOf("max-players=") + 12, +5)
        maxplay.Text = demxp

        'port conf
        deport = config.Text.Substring(config.Text.LastIndexOf("server-port=") + 12, +5)
        port.Text = deport

        'nether conf
        denether = config.Text.Substring(config.Text.LastIndexOf("allow-nether=") + 13, +5)
        deneth.Text = denether

        'PVP conf
        depvp = config.Text.Substring(config.Text.LastIndexOf("pvp=") + 4, +5)
        depv.Text = depvp


        'online conf
        deon = config.Text.Substring(config.Text.LastIndexOf("online-mode=") + 12, +5)
        deonline.Text = deon

        'white conf
        wl = config.Text.Substring(config.Text.LastIndexOf("white-list=") + 11, +5)
        wls.Text = wl
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        config.Text = config.Text.Replace("max-players=" & demxp, "max-players=" & maxplay.Text)
        demxp = maxplay.Text
        My.Computer.FileSystem.WriteAllText("server.properties", config.Text, False)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        config.Text = config.Text.Replace("server-port=" & deport, "server-port=" & port.Text)
        deport = port.Text
        My.Computer.FileSystem.WriteAllText("server.properties", config.Text, False)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        config.Text = config.Text.Replace("allow-nether=" & denether, "allow-nether=" & deneth.Text)
        denether = deneth.Text
        My.Computer.FileSystem.WriteAllText("server.properties", config.Text, False)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        config.Text = config.Text.Replace("pvp=" & depvp, "pvp=" & depv.Text)
        depvp = depv.Text
        My.Computer.FileSystem.WriteAllText("server.properties", config.Text, False)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        config.Text = config.Text.Replace("online-mode=" & deon, "online-mode=" & deonline.Text)
        deon = deonline.Text
        My.Computer.FileSystem.WriteAllText("server.properties", config.Text, False)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        config.Text = config.Text.Replace("white-list=" & wl, "white-list=" & wls.Text)
        wl = wls.Text
        My.Computer.FileSystem.WriteAllText("server.properties", config.Text, False)
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub APPLY_Click(sender As Object, e As EventArgs) Handles APPLY.Click
        On Error Resume Next
        If MsgBox("CONFIRM TO SAVE CHANGE AND RELOAD", MsgBoxStyle.YesNo, "CONFIRM") = vbYes Then
            My.Computer.FileSystem.WriteAllText("server.properties", config.Text, False)
            serverstart.StandardInput.WriteLine("restart")
            Me.Close()
        End If

    End Sub

    Private Sub Button7_Click_1(sender As Object, e As EventArgs)

        fr = My.Computer.FileSystem.OpenTextFileReader("server.properties")
            config.Text = fr.ReadToEnd
        fr.Close()


    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Me.Close()
    End Sub
End Class