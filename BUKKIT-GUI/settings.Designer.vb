﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(settings))
        Me.APPLY = New System.Windows.Forms.Button()
        Me.deneth = New System.Windows.Forms.ComboBox()
        Me.depv = New System.Windows.Forms.ComboBox()
        Me.deonline = New System.Windows.Forms.ComboBox()
        Me.wls = New System.Windows.Forms.ComboBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.port = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.maxplay = New System.Windows.Forms.TextBox()
        Me.config = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.BunifuDragControl1 = New Bunifu.Framework.UI.BunifuDragControl(Me.components)
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'APPLY
        '
        Me.APPLY.BackColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(217, Byte), Integer))
        Me.APPLY.FlatAppearance.BorderSize = 0
        Me.APPLY.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.APPLY.ForeColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.APPLY.Location = New System.Drawing.Point(-11, 207)
        Me.APPLY.Name = "APPLY"
        Me.APPLY.Size = New System.Drawing.Size(650, 34)
        Me.APPLY.TabIndex = 17
        Me.APPLY.Text = "APPLY ALL"
        Me.APPLY.UseVisualStyleBackColor = False
        '
        'deneth
        '
        Me.deneth.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.deneth.FormattingEnabled = True
        Me.deneth.Items.AddRange(New Object() {"true", "false"})
        Me.deneth.Location = New System.Drawing.Point(112, 95)
        Me.deneth.Name = "deneth"
        Me.deneth.Size = New System.Drawing.Size(113, 21)
        Me.deneth.TabIndex = 2
        '
        'depv
        '
        Me.depv.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.depv.FormattingEnabled = True
        Me.depv.Items.AddRange(New Object() {"true", "false"})
        Me.depv.Location = New System.Drawing.Point(88, 122)
        Me.depv.Name = "depv"
        Me.depv.Size = New System.Drawing.Size(137, 21)
        Me.depv.TabIndex = 2
        '
        'deonline
        '
        Me.deonline.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.deonline.FormattingEnabled = True
        Me.deonline.Items.AddRange(New Object() {"true", "false"})
        Me.deonline.Location = New System.Drawing.Point(88, 149)
        Me.deonline.Name = "deonline"
        Me.deonline.Size = New System.Drawing.Size(137, 21)
        Me.deonline.TabIndex = 2
        '
        'wls
        '
        Me.wls.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.wls.FormattingEnabled = True
        Me.wls.Items.AddRange(New Object() {"true", "false"})
        Me.wls.Location = New System.Drawing.Point(79, 180)
        Me.wls.Name = "wls"
        Me.wls.Size = New System.Drawing.Size(146, 21)
        Me.wls.TabIndex = 2
        '
        'Button6
        '
        Me.Button6.FlatAppearance.BorderSize = 0
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Location = New System.Drawing.Point(231, 178)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(105, 23)
        Me.Button6.TabIndex = 16
        Me.Button6.Text = "APPLY"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(7, 183)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "WHITELIST"
        '
        'Button5
        '
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Location = New System.Drawing.Point(231, 149)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(105, 23)
        Me.Button5.TabIndex = 13
        Me.Button5.Text = "APPLY"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(7, 154)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "ONLINE MODE"
        '
        'Button4
        '
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Location = New System.Drawing.Point(231, 120)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(105, 23)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "APPLY"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(7, 125)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "SERVER PVP"
        '
        'Button3
        '
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(231, 94)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(105, 23)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "APPLY"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(7, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "SERVER NETHER"
        '
        'Button2
        '
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Location = New System.Drawing.Point(231, 67)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(105, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "APPLY"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(7, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "SERVER PORT"
        '
        'port
        '
        Me.port.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.port.Location = New System.Drawing.Point(91, 69)
        Me.port.Multiline = True
        Me.port.Name = "port"
        Me.port.Size = New System.Drawing.Size(134, 20)
        Me.port.TabIndex = 5
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Button1.Location = New System.Drawing.Point(231, 41)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(105, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "APPLY"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(7, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "MAX PLAYER "
        '
        'maxplay
        '
        Me.maxplay.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.maxplay.Location = New System.Drawing.Point(91, 43)
        Me.maxplay.Multiline = True
        Me.maxplay.Name = "maxplay"
        Me.maxplay.Size = New System.Drawing.Size(134, 20)
        Me.maxplay.TabIndex = 2
        '
        'config
        '
        Me.config.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.config.Location = New System.Drawing.Point(342, 43)
        Me.config.Multiline = True
        Me.config.Name = "config"
        Me.config.Size = New System.Drawing.Size(277, 158)
        Me.config.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(108, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Button9)
        Me.Panel2.Location = New System.Drawing.Point(1, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(623, 24)
        Me.Panel2.TabIndex = 19
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.FromArgb(CType(CType(217, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Button9.FlatAppearance.BorderSize = 0
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Location = New System.Drawing.Point(575, -3)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(48, 27)
        Me.Button9.TabIndex = 19
        Me.Button9.UseVisualStyleBackColor = False
        '
        'BunifuDragControl1
        '
        Me.BunifuDragControl1.Fixed = True
        Me.BunifuDragControl1.Horizontal = True
        Me.BunifuDragControl1.TargetControl = Me.Panel2
        Me.BunifuDragControl1.Vertical = True
        '
        'settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(625, 241)
        Me.Controls.Add(Me.APPLY)
        Me.Controls.Add(Me.config)
        Me.Controls.Add(Me.deneth)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.depv)
        Me.Controls.Add(Me.deonline)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.wls)
        Me.Controls.Add(Me.maxplay)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.port)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "settings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SETTINGS SERVER"
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents maxplay As TextBox
    Friend WithEvents Button1 As Button
    Public WithEvents config As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents port As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Button5 As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents Button6 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents wls As ComboBox
    Friend WithEvents deonline As ComboBox
    Friend WithEvents depv As ComboBox
    Friend WithEvents deneth As ComboBox
    Friend WithEvents APPLY As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Button9 As Button
    Friend WithEvents BunifuDragControl1 As Bunifu.Framework.UI.BunifuDragControl
End Class
